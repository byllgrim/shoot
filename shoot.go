package main

import (
	"shoot/gam"
	"shoot/sys"
)

func main() {
	syssta := sys.Init()
	gamsta := gam.Init()

	for syssta.Out.Run {
		syssta = sys.Process(syssta, gamsta.Out)
		gamsta = gam.Process(gamsta, syssta.Out)
	}
}
