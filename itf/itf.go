package itf

type Sysout struct {
	Kbd Kbd
	Aim Aim
	Run bool
	Dir Fvec
}

type Gamout struct {
	Cir []Cir
	Amo int32
}

type Kbd struct {
	Bclk bool
}

type Aim struct {
	Pos  Ivec
	Trig bool
}

type Cir struct {
	Pos Ivec
	Rad int32
}

type Fvec struct {
	X float32
	Y float32
}

type Ivec struct {
	X int32
	Y int32
}
