package sys

import (
	"fmt"
	"flag"
	"github.com/veandco/go-sdl2/gfx"
	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
	"shoot/itf"
	"time"
)

type syssta struct {
	tim int64
	ren *sdl.Renderer
	fnt *ttf.Font
	lp  bool
	bp  bool
	grd bool
	Out itf.Sysout
}

const (
	stp int64 = 1000 / 25
)

func Init() syssta {
	sta := syssta{}

	grd := flag.Bool("grd", false, "enable grid")
	flag.Parse()
	sta.grd = *grd

	sta.tim = time.Now().UnixNano() / 1000000
	sta.ren = initSdl()
	sta.Out.Run = true

	ttf.Init()
	sta.fnt, _ = ttf.OpenFont(
		"Inconsolata-Regular.ttf",
		16)

	return sta
}

func Process(sta syssta, gamout itf.Gamout) syssta {
	// Sync time
	tim := syncTime(sta.tim)

	// Check quit status
	run := !isQuitting()

	// Poll inputs
	kbd, dir, bp := checkKeyboard(sta.bp)
	aim, lp := checkMouse(sta.lp)

	// Render
	clearScreen(sta.ren)
	drawGrid(sta.ren, sta.grd)
	drawObjects(sta.ren, gamout.Cir)
	drawTexts(sta, gamout)
	sta.ren.Present()

	// Return
	out := itf.Sysout{
		Run: run,
		Dir: dir,
		Kbd: kbd,
		Aim: aim,
	}
	newsta := syssta{
		ren: sta.ren,
		fnt: sta.fnt,
		tim: tim,
		bp:  bp,
		lp:  lp,
		grd: sta.grd,
		Out: out,
	}
	return newsta
}

func checkMouse(lp bool) (itf.Aim, bool) {
	x, y, s := sdl.GetMouseState()

	l := (s & sdl.BUTTON_LEFT) != 0
	trig := l && !lp
	pos := itf.Ivec{X: x, Y: y}
	aim := itf.Aim{Pos: pos, Trig: trig}

	return aim, l
}

func checkKeyboard(bp bool) (itf.Kbd, itf.Fvec, bool) {
	kst := sdl.GetKeyboardState()

	w := kst[sdl.SCANCODE_W] == 1
	a := kst[sdl.SCANCODE_A] == 1
	s := kst[sdl.SCANCODE_S] == 1
	d := kst[sdl.SCANCODE_D] == 1
	dir := calcDirection(w, a, s, d)

	b := kst[sdl.SCANCODE_B] == 1
	bclk := b && !bp
	kbd := itf.Kbd{Bclk: bclk}

	return kbd, dir, b
}

func calcDirection(w bool, a bool, s bool, d bool) itf.Fvec {
	up := w && !s
	dn := !w && s
	le := a && !d
	ri := !a && d

	x := float32(0.0)
	y := float32(0.0)
	switch true {
	case up && ri:
		x = 0.7071067811865475
		y = -0.7071067811865475
	case up && le:
		x = -0.7071067811865475
		y = -0.7071067811865475
	case dn && ri:
		x = 0.7071067811865475
		y = 0.7071067811865475
	case dn && le:
		x = -0.7071067811865475
		y = 0.7071067811865475
	case up:
		y = -1
	case dn:
		y = 1
	case le:
		x = -1
	case ri:
		x = 1
	}

	dir := itf.Fvec{
		X: x,
		Y: y,
	}

	return dir
}

func syncTime(nxt int64) int64 {
	nxt += stp

	cur := time.Now().UnixNano() / 1000000

	if cur < nxt {
		dly := uint32(nxt - cur)
		sdl.Delay(dly)
	}

	return nxt
}

func isQuitting() bool {
	qit := false
	evt := sdl.PollEvent()

	for evt != nil {
		switch evt.(type) {
		case *sdl.QuitEvent:
			qit = true
		}

		evt = sdl.PollEvent()
	}

	return qit
}

func initSdl() *sdl.Renderer {
	sdl.Init(sdl.INIT_EVERYTHING)

	win, _ := sdl.CreateWindow(
		"shoot",
		sdl.WINDOWPOS_UNDEFINED,
		sdl.WINDOWPOS_UNDEFINED,
		800,
		600,
		(sdl.WINDOW_SHOWN | sdl.WINDOW_RESIZABLE))
	ren, _ := sdl.CreateRenderer(win, -1, sdl.RENDERER_ACCELERATED)

	ren.SetDrawColor(255, 255, 255, 255)
	ren.Clear()
	ren.Present()

	return ren
}

func clearScreen(ren *sdl.Renderer) {
	ren.SetDrawColor(255, 255, 255, 255)
	ren.Clear()
}

func drawObject(ren *sdl.Renderer, cir itf.Cir) {
	col := sdl.Color{0, 0, 0, 255}
	rad := cir.Rad
	x := cir.Pos.X
	y := cir.Pos.Y

	gfx.FilledCircleColor(ren, x, y, rad, col)
}

func drawObjects(ren *sdl.Renderer, cir []itf.Cir) {
	for _, c := range cir {
		drawObject(ren, c)
	}
}

func drawGrid(ren *sdl.Renderer, grd bool) {
	col := sdl.Color{0, 0, 0, 255}  // TODO global "black" constant
	w, h, _ := ren.GetOutputSize()

	if !grd {
		return
	}

	for y := int32(0); y < h; y += 50 {
		gfx.HlineColor(ren, 0, w, y, col)
	}

	for x := int32(0); x < w; x += 50 {
		gfx.VlineColor(ren, x, 0, h, col)
	}
}

func drawTexts(sta syssta, gamout itf.Gamout) {
	drawStringAt("shoot", 0, 100, sta)

	amo := fmt.Sprintf("ammo: %02d", gamout.Amo)
	drawStringAt(amo, 25, 160, sta)

	drawStringAt("kill:  0", 50, 160, sta)
}

func drawStringAt(str string, y int32, w int32, sta syssta) {
	col := sdl.Color{0, 0, 0, 255}
	rec := sdl.Rect{0, y, w, 25}

	srf, _ := sta.fnt.RenderUTF8Solid(str, col)
	tex, _ := sta.ren.CreateTextureFromSurface(srf)

	sta.ren.Copy(tex, nil, &rec)
}
