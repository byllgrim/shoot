package gam

import (
	"math"
	"math/rand"
	"shoot/itf"
)

const (
	radply int32 = 25
	radbul int32 = 5
	plyspd int32 = 10
)

type gamsta struct {
	ply obj
	bts []obj
	bls []obj
	amo int32
	Out itf.Gamout
}

type obj struct {
	pos vec
	dir vec
	rad int32
}

type vec struct {
	x int32
	y int32
}

func Init() gamsta {
	ply := obj{
		rad: radply,
	}
	sta := gamsta{
		amo: 25,
		ply: ply,
	}

	return sta
}

func Process(sta gamsta, sysout itf.Sysout) gamsta {
	// Process input
	plydir := calcDirection(sysout.Dir)
	plypos := updatePosition(sta.ply.pos, plydir)
	btsnew := spawnBots(sysout.Kbd, sta.bts)
	blsnew, amo := spawnBullet(sysout.Aim, sta.ply, sta.bls, sta.amo)

	// Advance game
	btsply := playBots(btsnew)
	btsded := detectHits(btsply, blsnew)
	btspwn := despawnDeads(btsded, btsply)
	blspwn := despawnBullets(blsnew)
	bts := updatePositions(btspwn)
	bls := updatePositions(blspwn)
	ply := obj{
		pos: plypos,
		dir: plydir,
		rad: radply,
	}

	// Produce output
	out := output(ply, bts, bls, amo)

	newsta := gamsta{
		ply: ply,
		bts: bts,
		bls: bls,
		amo: amo,
		Out: out,
	}
	return newsta
}

func output(ply obj, bts []obj, bls []obj, amo int32) itf.Gamout {
	obs := []obj{}
	cir := []itf.Cir{}

	obs = append(obs, ply)
	obs = append(obs, bts...)
	obs = append(obs, bls...)
	for _, o := range obs {
		p := itf.Ivec{X: o.pos.x, Y: o.pos.y}
		c := itf.Cir{Pos: p, Rad: o.rad}
		cir = append(cir, c)
	}

	out := itf.Gamout{Amo: amo, Cir: cir}
	return out
}

func getRandDir() vec {
	x := rand.Int31n(20) - 10
	y := rand.Int31n(20) - 10

	return vec{x: x, y: y}
}

func limitRange(i int32, a int32, b int32) int32 {
	if i < a {
		return a
	} else if b < i {
		return b
	} else {
		return i
	}
}

func playBots(bts []obj) []obj {
	new := []obj{}

	for _, bot := range bts {
		rnd := (rand.Intn(100) == 1)

		x := limitRange(bot.pos.x, 0, 800)
		y := limitRange(bot.pos.y, 0, 600)
		newpos := vec{x: x, y: y}
		posdif := (newpos != bot.pos)

		if rnd || posdif {
			bot.dir = getRandDir()
		}

		bot.pos = newpos
		new = append(new, bot)
	}

	return new
}

func isObjInObs(obj obj, obs []obj) bool {
	for _, o := range obs {
		if obj == o {
			return true
		}
	}

	return false
}

func despawnDeads(ded []obj, bts []obj) []obj {
	liv := []obj{}

	for _, b := range bts {
		kill := isObjInObs(b, ded)

		if !kill {
			liv = append(liv, b)
		}
	}

	return liv
}

func detectHits(bts []obj, bls []obj) []obj {
	ded := []obj{}

	for _, bot := range bts {
		if hasHit(bot, bls) {
			ded = append(ded, bot)
		}
	}

	return ded
}

func hasHit(bot obj, bls []obj) bool {
	hit := false

	for _, bul := range bls {
		dx := math.Abs(float64(bot.pos.x - bul.pos.x))
		dy := math.Abs(float64(bot.pos.y - bul.pos.y))
		l := math.Sqrt(dx*dx + dy*dy)
		if l < float64(bot.rad+bul.rad) {
			hit = true
		}
	}

	return hit
}

func despawnBullets(bls []obj) []obj {
	new := []obj{}

	for _, b := range bls {
		xok := (0 <= b.pos.x) && (b.pos.x <= 10000)
		yok := (0 <= b.pos.y) && (b.pos.y <= 10000)
		if xok && yok {
			new = append(new, b)
		}
	}

	return new
}

func updatePositions(obs []obj) []obj {
	for i, o := range obs {
		obs[i].pos = updatePosition(o.pos, o.dir)
	}

	return obs
}

func updatePosition(pos vec, dir vec) vec {
	x := pos.x + dir.x
	y := pos.y + dir.y

	newpos := vec{x: x, y: y}

	return newpos
}

func calcDirection(nrm itf.Fvec) vec {
	x := int32(nrm.X * float32(plyspd))
	y := int32(nrm.Y * float32(plyspd))
	dir := vec{
		x: x,
		y: y,
	}

	return dir
}

func spawnBots(inp itf.Kbd, bts []obj) []obj {
	if inp.Bclk {
		posx := rand.Int31n(800)
		posy := rand.Int31n(600)
		dirx := rand.Int31n(20) - 10
		diry := rand.Int31n(20) - 10
		bot := obj{
			pos: vec{posx, posy},
			dir: vec{dirx, diry},
			rad: radply,
		}
		newbts := append(bts, bot)

		return newbts
	} else {
		return bts
	}
}

func spawnBullet(aim itf.Aim, ply obj, bls []obj, amo int32) ([]obj, int32) {
	if aim.Trig && (amo > 0) {
		aimpos := vec{x: aim.Pos.X, y: aim.Pos.Y}
		dif := calcScaledDiff(ply.pos, aimpos, 20)
		bul := obj{
			pos: ply.pos,
			dir: dif,
			rad: radbul,
		}
		newbls := append(bls, bul)
		newamo := amo - 1

		return newbls, newamo
	} else {
		return bls, amo
	}
}

func calcScaledDiff(a vec, b vec, s int) vec {
	dx := float64(b.x - a.x)
	dy := float64(b.y - a.y)

	l := math.Sqrt(dx*dx + dy*dy)
	xf := dx / l * float64(s)
	yf := dy / l * float64(s)

	return vec{int32(xf), int32(yf)}
}
