build:
	go build -v shoot.go

fmt:
	find | grep "\.go$$" | xargs -n1 go fmt
