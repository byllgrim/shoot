# shoot

Attempting to make a simple 2D top-down shooter.


Goal:

* Experiment with software design


Install deps:

* go get -v github.com/veandco/go-sdl2/sdl
* go get -v github.com/veandco/go-sdl2/gfx
* go get -v github.com/veandco/go-sdl2/ttf


Inspiration:

* https://store.steampowered.com/app/248610/Door_Kickers/
* http://www.cs2d.com/
* https://store.steampowered.com/app/219150/Hotline_Miami/
* https://en.wikipedia.org/wiki/SWAT_4


Ideas:

* 2D graphics
* Players with guns
* Fog of war
* Shroud of darkness
* Online multiplayer
* Audio effects
